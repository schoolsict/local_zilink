<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Defines the capabilities for the ZiLink block
 *
 * @package     block_zilink
 * @author      Ian Tasker <ian.tasker@schoolsict.net>
 * @copyright   2010 onwards SchoolsICT Limited, UK (http://schoolsict.net)
 * @copyright   Includes sub plugins that are based on and/or adapted from other plugins please see sub plugins for credits and notices. 
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once( dirname(__FILE__).'/security.php');
require_once( dirname(__FILE__).'/interfaces.php');

class ZiLinkData implements iZiLinkData {

    
    function __construct($courseid = null, $instanceid = null){
    
        global $DB;
        
        $this->security = new ZiLinkSecurity();
        $this->personalData = null;
        $this->globalData = new stdClass();
    }
    
    public function Security()
    {
        return $this->security;
    }
    
    public function GetPersonData($data_type = 'none', $idnumber,$required = false)
    {

        global $CFG,$DB,$USER;
       
        $data = new stdClass();
        
        if(!is_array($data_type))
        {
            if ($data_type == 'none')
            {
                return null;
            }
            
            $data_type = array($data_type);
        }
        
        foreach($data_type as $type)
        {
            $data->{$type} = null;
        }

        
        if($this->personalData == null)
        {
            $this->personalData = $DB->get_records('zilink_user_data');
        }

        if(count($this->personalData) > 0)
        {
            $matched = false;
            foreach($this->personalData as $record)
            {
                if($record->user_idnumber <> $idnumber)
                {
                    continue;
                }
                
                $matched = true;
                
                foreach($record as $field => $record_data)
                {
                   
                    if(in_array($field,$data_type) || in_array('all',$data_type)) 
                    {
                        
                        if($field == 'id')
                            continue;
                        if($field == 'user_idnumber' || $record_data == null)
                        {
                            $data->{$field} = new stdClass();
                            $data->{$field} = $record_data;
                        }
                        else 
                        {
                            //    $data = new stdClass();
                            $xml = str_replace('xmlns="http://zilinkplatform.net/timetable"', '',base64_decode($record_data));
                            $xml = str_replace('xmlns="http://zilinkplatform.net/assessment/gradesets"', '',$xml);
                            $xml = str_replace('xmlns="http://zilinkplatform.net/pictures"', '',$xml);
                            $xml = str_replace('xmlns="http://zilinkplatform.net/assessment/history"', '',$xml);
                            $xml = str_replace('xmlns:ns2="http://zilinkplatform.net/moodle2/person"', '',$xml);
                            $xml = str_replace('ns2:', '',$xml);
                            $data->{$field} = simplexml_load_string($xml,'simple_xml_extended');
                             
                            if(count($data->{$field}->count()) == 0)
                            {
                                $tmp = null;
                            }
                            
                            if($field == 'timetable' )
                            {
                                
                                try{
                                    if(isset($data->{$field}) || $data->{$field} <> null )
                                    {
                                        $tmp3 = $data->{$field}->timetable[(int)$CFG->zilink_timetable_offset];
                                        $data->{$field} =  @simplexml_load_string($tmp3->asXML(),'simple_xml_extended');
                                    }
                                    else 
                                    {
                                        $data->{$field} = null;
                                    }
                                } catch (Eception $e){
                                    
                                    $data->{$field} = null;
                                }
                                
                            } 
                            else if ($field == 'extended_details' )
                            {
                                if(isset($data->extended_details->LearnerPersonal))
                                    $data->extended_details = $data->extended_details->LearnerPersonal;
                                
                                if(isset($data->extended_details->WorkForcePersonal))
                                    $data->extended_details =  $data->extended_details->WorkForcePersonal;
                                
                                if(isset($data->extended_details->ContactPersonal))
                                    $data->extended_details =  $data->extended_details->ContactPersonal;
                            }
                        }
    
                        if($required && $data->{$field} == null)
                        {
                            throw new Exception("Missing Required Data");
                        }
                    }   
                }
            }
            if($required && $matched == false)
            {
                throw new Exception("Missing Required Data");
            }
        }
        else
        {
            if($required)
            {
                throw new Exception("Missing Required Data");
            }
        }   
        if(!empty($idnumber)) {
            $data->user = $DB->get_record('user',array('idnumber' => $idnumber));
        }
        return $data;
    }

    public function GetGlobalData($data_type = 'none',$required = false)
    {
        global $CFG,$DB,$USER;
        
        $data = null;
        $all = false;
        /*
        if(!is_array($data_type))
        {
            if ($data_type == 'none')
            {
                return null;
            }
            
            $data_type = array($data_type);
        }
        */
        
        if(strstr($data_type, '-all'))
        {
            $data_type = str_replace('-all','',$data_type);
            $all = true;
        }
        
        if(isset($this->globalData->{$data_type}) && is_object($this->globalData->{$data_type}))
        {
            return $this->globalData->{$data_type};
        }
        
        $sql = "select * from {zilink_global_data} where " . $DB->sql_compare_text('setting') . " = '".$data_type."'";
        if($record = $DB->get_record_sql($sql,null))
        {
            $xml = str_replace('xmlns="http://zilinkplatform.net/timetable"', '',$record->value);
            $xml = str_replace('xmlns="http://zilinkplatform.net/assessment/gradesets"', '',$xml);
            $xml = str_replace('xmlns="http://zilinkplatform.net/assessment/history"', '',$xml);
            
            if($data_type == 'timetable' ) {
                
                $tmp = new stdClass();
                $tmp = simplexml_load_string($xml,'simple_xml_extended');
                
                if($tmp <> null && $tmp->count() == 0)
                {
                    $tmp = null;
                }             
                
                if($all)
                {
                    $data = $tmp;
                    
                } else {
                    try{
                        if(($tmp <> null || isset($tmp->timetable)) && (int)$CFG->zilink_timetable_offset < $tmp->count())
                        {
                            $tmp3 = $tmp->timetable[(int)$CFG->zilink_timetable_offset];                
                            $data =  @simplexml_load_string($tmp3->asXML(),'simple_xml_extended');
                            $this->globalData->{$data_type} = $data;
                        }
                        else
                        {
                            $data = null;
                        }

                    } catch (Exception $e){
                        $data = null;
                    }
                }
            }
            else {
                $data = @simplexml_load_string($xml,'simple_xml_extended');
                $this->globalData->{$data_type} = $data;
            }
            
            if(is_object($data) && $data->count() == 0)
            {
                $tmp = null;
            }
            
        }   
        if($data == null && $required)
        {
            throw new Exception("Missing Global Timetable Data", 1);
        }
        return $data;
    }

    
    public function GetLinkedPeopleData($person_type,$data_types,$context = null)
    {
        global $DB;
        
        if(!is_array($data_types) && $data_types <> null)
        {
            $data_types = array($data_types);
        }
        
        $people = $this->Security()->GetLinkedPeople($person_type,$context);
        $data = array($person_type => null);
        
        foreach ($people as $person)
        {
            
            if($data_types == null)
            {
                $data[$person_type][$person->idnumber] = null;
            }
            else {
                foreach($data_types as $data_type)
                {
                    if(!isset($data[$person_type][$person->idnumber]))
                    {
                        $data[$person_type][$person->idnumber] = $this->GetPersonData($data_type,$person->idnumber);
                        $data[$person_type][$person->idnumber]->user = $DB->get_record('user', array('idnumber' => $person->idnumber));
                    }
                    else 
                    {
                        $data[$person_type][$person->idnumber]->{$data_type} = $this->GetPersonData($data_type,$person->idnumber)->{$data_type};
                    }
                }
            }
        }
        
        return $data;
    }
    
}

class simple_xml_extended extends SimpleXMLElement
{
    public    function    Attribute($name)
    {
        //if(count($this) == 0) {
        //    return (String)'';
        //}
        
                
        foreach($this->Attributes() as $key=>$val)
        {
            if($key == $name)
                return (string)$val;
        }
    }
}